const app = require("express")();

app.set("port", process.env.PORT || 5000);

app.get("/", (req, res) => {
  return res.send("funcionó :'3");
});

app.listen(app.get("port"), () => {
  console.log(`Server in port ${app.get("port")}`);
});
